# Parts List For Arcade Machine

-

| Description    |     QTY    |         Unit Price   |   Total Price |
| -------------  | -------    |     --------------   | --------------|
| Plexiglass 18  x 24 x 0.093 in.    |   1       |           10.00       |     10.00
| Sanded Pine Plywood
| 3/8in 4ft x 8ft |    5      |            20.00      |     100.00
| 2x2 8ft           |   10      |             4.00      |      40.00
| Screws Various Sizes          |    1      |            15.00      |      15.00
| Joystick          |    2      |            13.00      |      26.00
| Arcade Quality Buttons   |    8      |             5.00      |      40.00
| Power Strip       |    1      |             7.25      |       7.25
| Charger           |    1      |             4.93      |       4.93
| HDMI Cable        |    1      |             5.49      |       5.49
| Speakers          |    1      |            13.88      |      13.88
| 21.5 Inch Monitor |    1      |           124.95      |     124.95
| Keyboard          |    1      |            11.49      |      11.49
| Heat Sink         |    1      |             4.35      |       4.35
| Wifi Dongle       |    1      |             9.99      |       9.99
| 32GB SD Card      |    1      |            11.99      |      11.99
| Raspberry PI 2    |    1      |            40.94      |      40.94
| Professional Power thing | 1 | 6.00 | 6.00
| Powered Usb Hub | 1 | 20.00 | 20.00
| Blue Boxes | 1 | 4.00 | 4.00
| Power Recepticles | 1 | 4.00 | 4.00


