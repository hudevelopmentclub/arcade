# Arcade
This repository contains the files related to the construction of the
arcade cabinet from the CAD drawings of the cabinet to the electrical
diagrams to the parts list. This repository has it all.

# Software and File Saving Procedure
The standardization of the software used is very important to prevent file
corruption, and to make our lives easier, as such these procedures should
be used.

1. All 2 dimensional cad drawings shall be edited in [librecad][1], and
   saved in the `dxf` file format.
2. All electrical schematics shall be drawn using the [kicad eda][2]
   package. This must be honored. The exception to this rule is
   QElectrotech which was used early on for quick sketches.
3. All 3 dimensional models shall be created using [OpenSCAD][3]. There
   will be no exceptions to this rule.
4. All notes about this project that are saved in this repository must be
   written in markdown or plain text. There shall not be any binary files
   placed under version control in this repository.

These rules will help to maintain the git repository. Additionally all of
these software packages save their information as plain text which is
superior for saving stuff in version control.

[1]: http://librecad.org/cms/home.html
[2]: http://kicad-pcb.org/
[3]: http://www.openscad.org/index.html
