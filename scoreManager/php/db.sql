CREATE TABLE Player (
    id          INTEGER PRIMARY KEY,
    nickname    VARCHAR(32)
);

CREATE TABLE Game (
    id          INTEGER PRIMARY KEY,
    title       VARCHAR(32)
);

CREATE TABLE Score (
    id          INTEGER PRIMARY KEY,
    userid      INTEGER REFERENCES Player(id),
    gameid      INTEGER REFERENCES Game(id),
    score       INTEGER
);

CREATE VIEW HighScore AS
SELECT
    p.nickname as nick,
    g.title as game,
    s.score as score
FROM
    Score s
LEFT JOIN Game g ON s.gameid = g.id
LEFT JOIN Player p ON p.id = s.userid;
